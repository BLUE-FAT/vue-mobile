const IS_PROD = ["production", "prod"].includes(process.env.NODE_ENV);
const autoprefixer = require("autoprefixer");
const pxtorem = require("postcss-pxtorem");
const path = require("path");
const pkg = require("./package.json");

process.env.VUE_APP_VERSION = pkg.version;
process.env.VUE_APP_UPDATED_TIME = new Date().toLocaleString();

module.exports = {
  publicPath: IS_PROD ? process.env.VUE_APP_PUBLIC_PATH : "./",
  runtimeCompiler: true, // 是否使用包含运行时编译器的 Vue 构建版本
  productionSourceMap: !IS_PROD, // 生产环境的 source map
  devServer: {
    proxy: {
      "/api": {
        target: "http://10.10.0.59:18033",
        ws: true,
        changeOrigin: true
      }
    }
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          hack: `true; @import "${path.join(__dirname, "./src/assets/css/vant-variables.less")}";`
        }
      },
      sass: {
        prependData: `@import "~@/assets/css/common.sass"`
      },
      scss: {
        prependData: `@import "~@/assets/css/common.scss";`
      },
      postcss: {
        plugins: [
          autoprefixer(),
          pxtorem({
            rootValue: 37.5,
            propList: ["*", "!min-width"]
          })
        ]
      }
    }
  }
};
