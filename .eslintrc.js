const IS_PROD = ["production", "prod"].includes(process.env.NODE_ENV);

module.exports = {
  root: true,
  env: {
    node: true,
    browser: true,
    es6: true,
  },
  extends: ["plugin:vue/essential", "plugin:prettier/recommended", "eslint:recommended"],
  plugins: ['html', 'vue', 'prettier', 'import'],
  rules: {
    "no-console": IS_PROD ? "warn" : "off",
    "no-debugger": IS_PROD ? "warn" : "off",
    "no-unused-vars": [
      "warn",
      {
        ignorePattern: "^_",
      },
    ],
    "prettier/prettier": "warn",
    "arrow-parens": 0,
    avoidEscape: 0,
    "space-before-function-paren": 0,
    "generator-star-spacing": 0,
    semi: [0],
    indent: ["off", 2],
    "no-multi-spaces": "warn",
    "no-prototype-builtins": "warn",
    "no-undef": "warn",
    "prefer-const": "warn",
    "key-spacing": [
      0,
      {
        singleLine: {
          beforeColon: true,
          afterColon: true,
        },
        multiLine: {
          beforeColon: true,
          afterColon: true,
          align: "colon",
        },
      },
    ],
  },
  parserOptions: {
    parser: "babel-eslint",
  },
};
