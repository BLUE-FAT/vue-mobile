import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'amfe-flexible';
import '@/assets/image/iconfont/iconfont.css'
import '@/assets/image/iconfont/iconfont.js'
import './router/permission'
import { global } from "@/js/assets/globalConfig.js"
import permission from "@/assets/js/directive/permission"
import {
  Button,
  Image as VanImage,
  Form,
  Field,
  NavBar,
  Tabbar,
  TabbarItem,
  Icon,
  Overlay,
  PasswordInput,
  Grid,
  GridItem,
  Tab,
  Tabs,
  ActionSheet,
  Dialog,
  List,
  Cell,
  popup,
  Picker,
  NumberKeyboard,
  Toast,
  Empty, CellGroup
} from 'vant';

Vue.prototype._global = global; //引入全局变量

Vue.use(permission);//注册全局的鉴权指令 v-permission="['admin','teacher']"
Vue.use(Button);
Vue.use(VanImage);
Vue.use(Form);
Vue.use(Field);
Vue.use(NavBar);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Icon);
Vue.use(Overlay);
Vue.use(PasswordInput);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(ActionSheet);
Vue.use(Dialog);
Vue.use(List);
Vue.use(Cell);
Vue.use(popup);
Vue.use(Picker);
Vue.use(NumberKeyboard);
Vue.use(Toast);
Vue.use(Empty);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')