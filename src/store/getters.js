const getters = {
  token: state => state.user.token,
  roles: state => state.user.roles,
  userId: state => state.user.userId,
  permission_routes: state => state.permission.routes,
}
export default getters
