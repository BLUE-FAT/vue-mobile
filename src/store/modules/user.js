import { login, getInfo } from "@/api/user";
import router, { resetRouter } from "@/router/index";

const state = {
  token: sessionStorage.getItem("token") || null,
  roles: JSON.parse(sessionStorage.getItem("roles")) || [],
  userId: null
};

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles;
    sessionStorage.setItem("roles", JSON.stringify(roles));
  },
  SET_USERID: (state, userId) => {
    state.userId = userId;
  }
};

const actions = {
  // user login
  login({ commit }, data) {
    return new Promise((resolve, reject) => {
      login(data)
        .then(response => {
          commit("SET_TOKEN", response.token);
          sessionStorage.setItem("token", response.token);
          sessionStorage.setItem("refreshToken", response.refreshToken);
          sessionStorage.setItem("timestamp", response.timestamp);
          resolve();
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // get user info
  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
      getInfo()
        .then(response => {
          const { data } = response;
          if (!data) {
            return reject("验证失败，请重新登录");
          }

          const { roleKeys, id } = data.user;
          if (!roleKeys || roleKeys.length <= 0) {
            return reject("no-rules");
          }
          commit("SET_ROLES", roleKeys);
          commit("SET_USERID", id);
          resolve(data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // user logout
  logout({ commit }) {
    return new Promise((resolve, reject) => {
      commit("SET_TOKEN", "");
      commit("SET_ROLES", []);
      commit("course/SET_COURSEID", null, { root: true });
      sessionStorage.clear();
      resetRouter();
      router.push("/login", () => {
        location.reload();
      });
      resolve();
    }).catch(error => {
      Promise.reject(error);
    });
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit("SET_TOKEN", "");
      commit("SET_ROLES", []);
      sessionStorage.clear();
      resetRouter();
      resolve();
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
